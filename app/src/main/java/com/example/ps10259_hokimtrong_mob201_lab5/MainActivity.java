package com.example.ps10259_hokimtrong_mob201_lab5;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView lvNews;
    NewsAdapter newsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvNews=findViewById(R.id.lvNews);
        NewsSeedAsyncTask newsSeedAsyncTask=new NewsSeedAsyncTask();
        newsSeedAsyncTask.execute();

    }

    class NewsSeedAsyncTask extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url=new URL("https://www.24h.com.vn/upload/rss/trangchu24h.rss");
                HttpURLConnection httpURLConnection= (HttpURLConnection) url.openConnection();
                InputStream inputStream=httpURLConnection.getInputStream();
                final List<News> newes=NewsReader.listNews(inputStream);
                newsAdapter=new NewsAdapter(MainActivity.this,newes);
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lvNews.setAdapter(newsAdapter);
                        lvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                News news=newes.get(position);
                                Intent intent=new Intent(MainActivity.this,NewsWebViewActivity.class);
                                Bundle bundle=new Bundle();
                                bundle.putString("link",news.getLink());
                                intent.putExtra("new",bundle);
                                startActivity(intent);
                            }
                        });
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

}
